# Commissioning detectors at CMS

## Overview

This page outlines the procedures for commissioning at Point 5. The
output of all code run should be stored in elogs under
`Home > Subsystem > GEM > P5 > Commissioning`. For long outputs, the
text can be stored on a log file on the P5 computer, but the name must
be clearly recorded in the elog. Sometimes during commissioning it will
be necessary to route fibers to different CTP7s. Whenever a fiber
intervention is made it should be recording in a commissioning elog. If
a fiber is swapped **be sure to update** `system_specific_constants.py`
see `gemos-usage-chamber-info`{.interpreted-text role="ref"}. If a CTP7
is moved or added, that should be recorded under
`Home > Subsystem > GEM > P5 > DAQ`. The top of commissioning elogs
should start with a summary for people who are not involved in
commissiong; for example "chamber X passed the following tests". This
should be followed by a list of the current configuration at P5 which is
listed in `system_specific_constats.py`; for example:

``` python
(1,7,0):"GE11-M-13L1-S",  
(1,7,1):"GE11-M-13L2-S",  
(1,7,2):"GE11-M-14L1-L",  
(1,7,3):"GE11-M-14L2-L",  
(1,7,4):"GE11-M-15L1-S",  
(1,7,5):"GE11-M-15L2-S",  
(1,7,6):"GE11-M-16L1-L",  
(1,7,7):"GE11-M-16L2-L",  
```

!!! tip
    Always run these commissioning tests in a screen session.

## Checking the AMC13 clock source

At point 5, during run time the AMC13 will recieve a TTC stream from
TCDS. However, during shutdown TCDS can have interruptions. Therfore,
sometimes it is necessary to use the AMC13 TTC loop back mode. At the
start of a commissioning shift, be sure to check elogs under
`Home > Subsystems > P5 > DAQ` for the most recent clock mode. If
changing clock modes is required, contact an expert. If an expert
changes the clock mode, then be sure to update the elog!

## Commissioning phase 0: check communication and fiber power

In this early stage of commissioning, the detectors are being installed.
There will be no cooling, no gas, and possibly no DCS. If there is no LV
DCS yet, then the help of DCS expert will be needed. The DCS expert will
need to manually power on chambers with a bench top power supply. Since
there is no cooling, it is prudent that the detectors be powered on for
as little time as possible! And this time must be **less than 30
minutes**!!! Any longer and the detector may be damaged, so a running
clock should be kept everytime a chamber is powered.

In this stage of commissioning, there are three tests that must be
completed.

### Test 1: fiber power test

The power of the optical fibers must be tested to ensure that:

1.  The mappings are correct
2.  None of the fibers were damaged during installation/cabling

Evaladas wrote a software tool on the CTP7 so that this can be done
quickly. To use it:

1.  Log onto the desired CTP7 as gemuser

2.  Execute the following command: (where the OHNUMBER refers to the
    link number of the chamber being tested on that CTP7)

    ``` bash
    /mnt/persistent/gemdaq/python/reg_interface/fiber_test.py -oh OHNUMBER
    ```

A print out from this command should look like this:

``` bash
GBT0 RX power: 438uW
GBT1 RX power: 371uW
GBT2 RX power: 453uW
TRIG0 RX power: 338uW
TRIG1 RX power: 317uW
```

A value lower than 110uW indicates a bad fiber and should be reported to
experts. If a value is 0uW, this indicates a bad mapping. If this
happens, then:

1.  Go to USC and ensure that the fibers are routed correctly into the
    CTP7 (for GBT links) and patch panel (for trigger links)
2.  Check that the OHNUMBER inserted correctly maps to the chamber that
    is powered
3.  If the issue is unresolved, then report this to the experts

### Test 2: communication test

This tests that the backend electronics can successfully communicate
with the frontend electronics. This is done using `testConnectivity.py`.
To perform this test:

1.  Determine the `shelf` number of your μTCA crate,

2.  Determine the `SLOT` of your AMC in the μTCA crate with the shelf
    number from step 2,

3.  Determine the `ohMask` of your detector(s) on your AMC in slot
    `SLOT`,

    !!! note
        Here `ohMask` is a 12-bit number, where a 1 in the $N^{th}$ bit
        means "consider this OptoHybrid." So an `ohMask = 0xc4c` would
        mean to use OptoHybrids 2, 3, 6, 10 and 11.

4.  Then execute:

    ``` bash
    testConnectivity.py --skipDACScan --skipScurve SHELF SLOT OHMASK 2>&1 | tee -a connectivityLog.log
    ```

!!! tip
    The `tee -a` option will prevent potentially important information from
    being overwritten.

For each OptoHybrid in `ohMask` this will:

1.  Check GBT communication & program GBTs
2.  Check SCA communication & reset SCAs
3.  Program FPGA & Check Communication
4.  Scan GBT phases & set each VFAT to a good phase
5.  Synchronize all VFATs and check VFAT communication

### Test 3: check VFAT info

The last test of this phase of commissioning is to check the VFAT
information (after doing `testConnectivity.py`). To do this execute:

``` bash
vfat_info_uhal.py -sSLOT --shelf=SHELF -gOHNUMBER
```

This will print out information from the VFAT. The shifter should check
that the correct version of AMC and OH firmware is installed. As
*always* the output should be stored in the elog.

## Commissioning phase 1: calibrating and configuring chambers

This phase of commissioning extensively tests the LV electronics on the
chamber, similar to QC7. The goal is to insure that the installed
chambers are healthy. For these tests to be performed **cooling must be
used**!!! Ideally LV DCS should be used, but a bench top power supply
can also be used *with the help of a DCS expert*.

This stage of commissioning may need to be repeated after christmas
shutdowns.

### Test 1: configuring super chambers

This step will be the first time that the DAC values are scaned at point
5 and will prepare the chamber for further tests.

1.  Begin by repreforming a GBT phase scan:

    ``` bash
    testConnectivity.py --skipDACScan --skipScurve --writePhases2File SHELF SLOT OHMASK 2>&1 | tee -a connectivityLog.log
    ```

2.  Get Calibration information from the Data Base:

    ``` bash
    getCalInfoFromDB.py SHELF SLOT OHNUMBER --write2File --write2CTP7
    ```

3.  Repreform a GBT phase scan and preform a DAC scan:

    ``` bash
    testConnectivity.py --acceptBadDACFits --skipScurve --writePhases2File SHELF SLOT OHMASK 2>&1 | tee -a connectivityLog.log
    ```

    !!! note
        Many of the chambers on the minus endcap were installed before the
        usage of DAC fit test for VFATs. These chambers still operate
        correctly, but will fail the DAC fit test. Thus, be sure to use the
        `--acceptBadDACFits` flag.

    !!! tip
        The DAC scan may fail at this point and one of the most common
        reasons for this occurence is that reference voltages on the CTP7
        did not get correctly updated from the data base. If the DAC scan
        fails, check that the values on the CTP7 match the values coming
        from the `getCalInfoFromDB.py`

4.  Configure the chamber with CFG_THR_ARM_DAC = 100:

    ``` bash
    confChamber.py --shelf=SHELF -sSLOT -gOHNUMBER --zeroChan
    ```

### Test 2: SCurves

Taking SCurves at point 5 is a big deal and this is one of the most
important tests during commissioning.

1.  Always start by configuring the chamber as a previous test may have
    altered the CFG_THR_ARM_DAC:

    ``` bash
    confChamber.py --shelf=SHELF -sSLOT -gOHNUMBER --zeroChan
    ```

2.  Run the scurve scan:

    ``` bash
    run_scans.py scurve SHELF SLOT OHMASK
    ```

3.  Analyze the scurve scan using the scandate reported in
    `run_scans.py`:

    ``` bash
    ana_scans.py scurve -s SCANDATE --chamberConfig --heavy -c
    ```

!!! tip
    The analysis of the scurves can be done in parallel with taking SBit
    rate scans. This is a huge time saver.

!!! note
    One of the most common problems at point 5 is with bad DAC values. When
    the analysis finishes, look carefully at the scurves. If the scurves are
    jumping all over the place, this most likely indicates bad DAC values.
    To fix this, manually write the DAC values from the
    `testConnectivity.py` output to the CTP7.

### Test 3: SBit threshold

Taking SBit threshold scan is used to identify one of the most common
problems caused by transportation/installation, shifted electronics.

1.  Always start by configuring the chamber, do it again here just to
    keep the habit:

    ``` bash
    confChamber.py --shelf=SHELF -sSLOT -gOHNUMBER --zeroChan
    ```

2.  Run the SBit threshold scan:

    ``` bash
    run_scans.py sbitThresh SHELF SLOT OHMASK
    ```

3.  Analyze the SBit threshold scan using the path given by
    `run_scans.py`:

    ``` bash
    ana_scans.py sbitThresh -i /path/to/SBitRateData.root --chamberConfig -m 100
    ```

A bad SBit threshold scan will show a flat (or nearly flat) line across
the plot. This is due to a problem with one (or more) of the SBits. If
this occurs, it is the commissioners responsibility to identify and
report the the bad SBits. Each VFAT has 8 SBits and if the connectors on
the VFAT has shifted at all, this will cause the loss of one (or more)
SBits. To identify the SBits, use the `gem_reg.py` tool.

1.  Configure the Chamber with a high CFG_THR_ARM_DAC (like 150):

    ``` bash
    confChamber.py --shelf=SHELF -sSLOT -gOHNUMBER --vt1=150
    ```

2.  Use the `gem_reg.py` tool on the correct CTP7:

    ``` bash
    gem_reg.py
    connect gem-shelfSHELF-amcSLOT
    ```

3.  Then mask all 8 sbits to make sure that the rate is 0. Here is an
    example, be sure to change OH and VFAT numbers:

    ``` bash
    gem-shelf01-amc05 > write GEM_AMC.OH.OH2.FPGA.TRIG.CTRL.TU_MASK.VFAT13_TU_MASK 0xff
    Initial value to write: 255, register GEM_AMC.OH.OH2.FPGA.TRIG.CTRL.TU_MASK.VFAT13_TU_MASK
    0x0000ff00(255) written to GEM_AMC.OH.OH2.FPGA.TRIG.CTRL.TU_MASK.VFAT13_TU_MASK

    gem-shelf01-amc05 > read GEM_AMC.OH.OH2.FPGA.TRIG.CNT.VFAT13_SBITS
    0x65088090 r    GEM_AMC.OH.OH2.FPGA.TRIG.CNT.VFAT13_SBITS               0x00000000
    ```

4.  Now, turn on individual SBits and to find the bad one(s). Make sure
    to do this for all 8 as there may be more than one. Here is an
    example of one bad SBit:

    ``` bash
    gem-shelf01-amc05 > write GEM_AMC.OH.OH2.FPGA.TRIG.CTRL.TU_MASK.VFAT13_TU_MASK 0xdf
    Initial value to write: 223, register GEM_AMC.OH.OH2.FPGA.TRIG.CTRL.TU_MASK.VFAT13_TU_MASK
    0x0000df00(223) written to GEM_AMC.OH.OH2.FPGA.TRIG.CTRL.TU_MASK.VFAT13_TU_MASK

    gem-shelf01-amc05 > read GEM_AMC.OH.OH2.FPGA.TRIG.CNT.VFAT13_SBITS
    0x65088090 r    GEM_AMC.OH.OH2.FPGA.TRIG.CNT.VFAT13_SBITS               0x02638233
    ```

Again make sure to record and report all the bad SBits. If a chamber has
too many bad SBits, it may need to be extracted.

## Commissioning phase 2: DCS mapping

Once the DCS is operational, the mappings must be tested. First for LV
and then for HV. To test LV mapping:

1.  Power on an individual chamber. Then use `gem_reg.py` to check that
    the correct chamber is on:

    ``` bash
    gem_reg.py
    connect gem-shelfSHELF-amcSLOT
    gem-shelf01-amc05 > rwc SCA*READY
    ```

2.  Make sure that the hex number output matches the expected OHNUMBER

To test the HV mapping:

1.  Technicians test the connections between the detector and patch
    panel
2.  The patch panel should be tested by experts by turning on channels
    at 10uA and checking with a multimeter

## Commissioning phase 3: high voltage training

!!! warning
    This procedure is in the process of being discussed and modified by the
    GE11 electronics group

The HV training can be performed before chambers have undergone phase 1.
Note that the HV training requires DCS, so Phase 2 must be completed
before you can begin. Make sure that **no LV is powered on** during HV
training and alert everyone (via mattermost) when HV training is
started.

1.  Flush the chambers with pure CO2 (18 L/h)
2.  Perform an initial check with 50V across the foils to spot potential
    short circuits. Discuss with experts if a short is observed.
3.  Train GEM foils one by one:
    -   Ramping at 3 V/s up to 600 V across GEM 1
    -   All other foils and gaps kept **off**
    -   Note the stable voltage achieved (no trips)
    -   repeat for GEM 2 and 3
4.  Stablize the Drift, Transfer, and Induction gaps (for at least 4 h)
    -   Drift at 900 V
    -   Transfer 1 and 3 and Induction at 600 V
    -   Transfer 2 at 800 V
5.  Turn all fields on with Drift at 3670 V for at least 12 h
    -   Equivalent divider current of 800 uA
