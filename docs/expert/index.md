# Expert guide

This site complements the user guides , and is intended to serve as a reference
for the GEM DAQ on-call experts, teststand responsibles, and admins of remote
teststands. It contains useful information on setting up a teststand from
scratch, debugging software and hardware issues, and generally more detailed
information than is necessary for operating the GEM DAQ system (hardware,
firmware, and software).
